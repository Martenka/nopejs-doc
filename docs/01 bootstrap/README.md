# Intro

How easily one can start with the app? Knowing the language itself (Java or TypeScript), one can generate a working project using web page or cli:

 - Java: https://start.spring.io/
 - NodeJS/NestJS: https://docs.nestjs.com/first-steps

# Project structure

// DI comparison - różne metody i ograniczenia

## Java
The Java project uses common structure for maven and gradle projects, separating tests from the main source code, including specific resources that are needed to run the app (like config). Classes are grouped into **packages**, one class per one file (in principle), name of the class is equal to name of the file. Project shape is described in pom.xml (dependencies, name, version, modules if any, build lifecycle). The generated project also contains maven wrapper, which will not be discussed here (https://maven.apache.org/wrapper/).

## NestJS
The NestJS project brings simple folder structure, unit tests are together with production code, e2e tests are separated. Classes are grouped into **folders**. There is no one rule or convention for naming classes and files, you can mix in files different language constructs (classes, types, functions). Basic setup contains additional config files: .eslintrc.js, .prettierrc, nest-cli.json, tsconfig.build.json, tsconfig.json and package.json (which is similar to pom.xml). 

# main

## Java
Entrypoint of the application is mainly declarative, `@SpringBootApplication` does the trick. When using more features (like scheduling, async elements, kafka and so on) one will add annotations. Exposed port has default value, one can change it with configuration.

## NestJS
Purely imperative entrypoint, explicitly set port with const value.

# Out-of-the box features

Both solutions provide typical web-development elements like controllers and services with dependency injection, tests with libraries, with a bit of (de)serialization abilities. One has to spot that NestJS uses modules configuration (app.module.ts), explicitly marking boundaries for set of classes. Declarative annotations (decorators in NestJS) are very similar (for services and controllers). Java application brings basic configuration (application.properties), many features are driven by this file, without any coding.

# Dependency injection

Both frameworks bring DI capabilities, but with different mechanisms behind. This aspect could state its own paper, but we're focusing only on the main characteristics and typical usage.

## Java

Bean
Autowiring
Bean configuration & redefinition
Constructor, setter, field injection
Configuring dependencies

## NestJS

Modules and injectables
Constructor injection
Problem with interfaces, use of tokens
Configuring dependencies

# Running the apps

## Java

In Java you need just one command, dependencies will be installed before start.

To run the server:
`mvn spring-boot:run`

## NestJS

One need to install the application before start: `yarn install`

To run the server:
`yarn start`

# Result

Both projects are similar with small differences what is provided (main, controller, service, test). We've added a few elements just to make projects comparable, containing almost the same classes: main, controller, service and tests (unit + e2e).

First result:

```
pmartenka@vostro:~/dev/repos/nopejs/apps$ sloc -f cli-table -e "(node_modules|target|dist)" .
┌───────────┬──────────┬────────┬─────────┬─────────────────────┬───────────────┬───────┬─────────────────────┬───────┬───────┐
│ Extension │ Physical │ Source │ Comment │ Single-line comment │ Block comment │ Mixed │ Empty block comment │ Empty │ To Do │
├───────────┼──────────┼────────┼─────────┼─────────────────────┼───────────────┼───────┼─────────────────────┼───────┼───────┤
│ - Total - │ 222      │ 181    │ 1       │ 0                   │ 1             │ 1     │ 0                   │ 41    │ 0     │
├───────────┼──────────┼────────┼─────────┼─────────────────────┼───────────────┼───────┼─────────────────────┼───────┼───────┤
│ xml       │ 44       │ 42     │ 1       │ 0                   │ 1             │ 1     │ 0                   │ 2     │ 0     │
├───────────┼──────────┼────────┼─────────┼─────────────────────┼───────────────┼───────┼─────────────────────┼───────┼───────┤
│ ts        │ 84       │ 71     │ 0       │ 0                   │ 0             │ 0     │ 0                   │ 13    │ 0     │
├───────────┼──────────┼────────┼─────────┼─────────────────────┼───────────────┼───────┼─────────────────────┼───────┼───────┤
│ java      │ 94       │ 68     │ 0       │ 0                   │ 0             │ 0     │ 0                   │ 26    │ 0     │
└───────────┴──────────┴────────┴─────────┴─────────────────────┴───────────────┴───────┴─────────────────────┴───────┴───────┘
```


# Conclusion

It's similar effort to start working with both technologies, but one have much more configuration to maintain in NestJS (all js and json files alongside package.json).