# Intro

Logging is the basic and crucial feature of any software framework. One should expect at least good set of functions and features out-of-the box.

## Application startup & basic shape

### Java/SpringBoot

The basic format consists of timestamp, with milliseconds, log level, PID, thread, package.Class and message.

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.0.5)

2024-01-03T14:20:23.095+01:00  INFO 437325 --- [           main] eu.espeo.demo.DemoApplication            : Starting DemoApplication using Java 20.0.1 with PID 437325 (/home/pmartenka/dev/repos/nopejs/apps/java/demo/target/classes started by pmartenka in /home/pmartenka/dev/repos/nopejs/apps/java/demo)
2024-01-03T14:20:23.097+01:00 DEBUG 437325 --- [           main] eu.espeo.demo.DemoApplication            : Running with Spring Boot v3.0.5, Spring v6.0.7
2024-01-03T14:20:23.098+01:00  INFO 437325 --- [           main] eu.espeo.demo.DemoApplication            : No active profile set, falling back to 1 default profile: "default"
2024-01-03T14:20:23.686+01:00  INFO 437325 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2024-01-03T14:20:23.695+01:00  INFO 437325 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2024-01-03T14:20:23.695+01:00  INFO 437325 --- [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.7]
2024-01-03T14:20:23.760+01:00  INFO 437325 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2024-01-03T14:20:23.763+01:00  INFO 437325 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 622 ms
2024-01-03T14:20:24.015+01:00  INFO 437325 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2024-01-03T14:20:24.023+01:00  INFO 437325 --- [           main] eu.espeo.demo.DemoApplication            : Started DemoApplication in 1.208 seconds (process running for 1.419)

```

### TS/NestJS
The basic format consists of **timestamp without milliseconds**, log level, class and message. Because of the project shape (folders only), there is **no such thing like package** in the log message, and of course there is no thread.

```
pmartenka@vostro:~/dev/repos/nopejs/apps/nodejs/demo$ yarn start
yarn run v1.22.19
warning ../../../../../../package.json: No license field
$ nest start
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [NestFactory] Starting Nest application...
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [InstanceLoader] AppModule dependencies initialized +13ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RoutesResolver] AppController {/}: +10ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/exceptions/handled, GET} route +3ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/exceptions/unhandled, GET} route +1ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/:id, GET} route +0ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/, GET} route +1ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/greetings, POST} route +0ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RoutesResolver] AppControllerWithoutFilter {/}: +1ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [RouterExplorer] Mapped {/no-filter/exceptions, GET} route +0ms
[Nest] 437314  - 01/03/2024, 2:20:22 PM     LOG [NestApplication] Nest application successfully started +2ms
```

## Requests processing

Example is based on a two get requests.

### Java/SpringBoot

`curl localhost:8080`
`curl localhost:8080`

```
2024-01-03T14:28:02.055+01:00  INFO 437325 --- [nio-8080-exec-2] eu.espeo.demo.basic.rest.AppController   : getAll()
2024-01-03T14:28:02.055+01:00 DEBUG 437325 --- [nio-8080-exec-2] eu.espeo.demo.basic.AppService           : getAll()
2024-01-03T14:28:02.055+01:00 DEBUG 437325 --- [nio-8080-exec-2] eu.espeo.demo.basic.AppService           : getHello()
2024-01-03T14:28:52.940+01:00  INFO 437325 --- [nio-8080-exec-4] eu.espeo.demo.basic.rest.AppController   : getAll()
2024-01-03T14:28:52.941+01:00 DEBUG 437325 --- [nio-8080-exec-4] eu.espeo.demo.basic.AppService           : getAll()
2024-01-03T14:28:52.941+01:00 DEBUG 437325 --- [nio-8080-exec-4] eu.espeo.demo.basic.AppService           : getHello()
```

Each request gets a **thread** from a pool so one can **easily track** from request processing start down to business logic, each request separately; compare: `[nio-8080-exec-2] eu.espeo.demo.basic.AppService           : getHello()` with `[nio-8080-exec-4] eu.espeo.demo.basic.AppService           : getHello()`.

### TS/NestJS

`curl localhost:3000`
`curl localhost:3000`

```
[Nest] 437314  - 01/03/2024, 2:28:13 PM     LOG [AppController] getAll()
[Nest] 437314  - 01/03/2024, 2:28:13 PM   DEBUG [AppService] getAll()
[Nest] 437314  - 01/03/2024, 2:28:13 PM   DEBUG [AppService] getHello()
[Nest] 437314  - 01/03/2024, 2:28:56 PM     LOG [AppController] getAll()
[Nest] 437314  - 01/03/2024, 2:28:56 PM   DEBUG [AppService] getAll()
[Nest] 437314  - 01/03/2024, 2:28:56 PM   DEBUG [AppService] getHello()
```

Without additional tracing/info one **cannot distinguish between request processing** stemmed from different requests. When there is only sequential processing the lack of tracing in the log is not a problem.

Co się stanie gdy?

curl -X POST localhost:3000/greetings -H 'Content-Type: application/json' -d '{"title": "bafjkds", "name": "wonsz"}'

title nie pasuje do enuma