# Intro

Assumptions and basic shape of the error/exception handling built into framework and language has significant impact on the design of production code.

## Basic try-catch

Both languages and frameworks provide exception mechanism, Java distinguishes checked and unchecked exceptions. The `try-catch` clause has similar shape, but in JS/TS catch is always of type `any`, where Java expects exception type (which can be more or less specific). With JS/TS one need to **manually distinguish between exception types**.

### Java

```
public class Exception1 extends RuntimeException() {}

public class Exception2 extends RuntimeException() {}

public class Exception3 extends RuntimeException() {}

public class Example {
    public void someFunctionality() {
        try {
            possibleException();
        } catch (Exception1 e) {
            // do some stuff
        } catch (Exception2 | Exception3 e) {
            // do more stuff
        }
    }
    
    public void possibleException() {
        // throw something
    }
}
```

### NodeJS

```
class Exception1 extends Error {}
class Exception2 extends Error {}
class Exception3 extends Error {}

class Example {
    public someFunctionality(): void {
        try {
            this.possibleException();
        } catch (e) {
            if (e instanceof Exception1) {
                // do some stuff
            } else if (e instanceof Exception2 || e instanceof Exception3) {
                // do more stuff
            }
        }
    }

    public possibleException(): void {
        // throw something
    }
}
```

## Webserver error handling

SpringBoot and NestJS are prepared to help with web-related error handling, like business-to-HTTP exception translation for great separation of layers. SpringBoot provides a few ways of error handling, but the presented one is a controller advice. NestJS brings exception filters.

### Default error handling

### Error handler/filter

#### Java
In SpringBoot one may use `@ControllerAdvice` with `@ExceptionHandler`. This allows to create consolidated place for error handling, **without inheritance**, **fully declarative**.

To be noted: in the `logger.warn` (or other log level) method, the last parameter is prepared to pass exceptions.

```
@ControllerAdvice
public class AppExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler
    protected ResponseEntity<String> handleExample(ExemplaryException exception) {
        logger.warn("handleExample", exception);
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body("Exemplary exception");
    }

}
```

Other way:

```
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    @ResponseBody
    protected String handleExample(ExemplaryException exception) {
        logger.warn("handleExample", exception);
        return "Exemplary exception";
    }
```

The latter brings more annotations, but the handling logic is simplified and clear.

#### NestJS

NestJS brings the idea of filters, that are declared as classes **implementing interface** `ExceptionFilter`, with additional decorator `@Catch`. The declared filter need to be pointed in a controller with `@UseFilters`.

```
import { Response } from 'express';

@Catch(ExemplaryException)
export class AppExceptionFilter implements ExceptionFilter<ExemplaryException> {
  private readonly logger = new Logger(AppExceptionFilter.name);

  catch(exception: ExemplaryException, host: ArgumentsHost) {
    this.logger.warn(exception.stack);
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    response.status(HttpStatus.NOT_IMPLEMENTED).send('Exemplary exception');
  }
}
```

To send proper response one need to use builder from **underlying `express` framework**. In NestJS we need to mix more low level elements (like `Response` instance) to shape the response from the handler.
