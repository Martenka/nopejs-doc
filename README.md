# Goal

Compare popular technologies and frameworks for web development: NodeJS + NestJS/TS vs Java + SpringBoot.

# Intro

Web development can be run with different tech stacks. Java and NodeJS are popular choices, with different characteristics and pros and cons. Each technology should be chosen on purpose, with all important consequences.   

This comparison assumes advanced knowledge about programming languages and frameworks.

# Plan

Areas of interest:

1. Simplicity of bootstrapping,//podstawowy kształt i różnice, szybkość budowania aplikacji
2. Basic logging abilities,//prosty logger, ale też format logów
3. Error handling,//szczególnie przy wielu żądaniach
4. Configuration,//.dotenv vs application, @Value, @ConfigurationProperties itp.
5. Testing abilities,//jak łatwo napisać testy, parametryczne, mocki, webowe + zależności typu hibernate/h2, testcontainers
6. Dockerization,//budowanie w kontenerze, jak skomplikowany Dockerfile, rozmiar obrazu, użycie zasobów kontenera
7. Data serialization in REST and kafka,//co dostajemy z pudełka
8. ORM,//popularne rozwiązania
9. DB schema migration,//popularne rozwiązania
10. Kafka integration,//
11. Multimodule setup,// monorepo, nx, lerna vs pom
12. k8s integration,
13. Tooling and IDEs.

# Metrics

1. Lines of code,
2. Docker Image size,
3. Performance, memory & cpu usage,
4. Size of the project/bundle.

# Additional tools

1. Lines of code: sloc, version: 0.2.1,
2. docker stats,
3. Artillery scripts,
4. IDEs: IntelliJ, VS Code.

# Caveats

Each technology and framework will be used with the smallest set of additional libraries. Some of the cons of the framework or language (i.e. lack of suitable data types) can be fixed with additional libraries or frameworks, but here we focus on rather basic setup. Still, one could make different choices.

# Testing setup

For docker containers versions will be derived from the base image or Dockerfile.

## Java

 - Java version: 17
 - OpenJDK Runtime Environment Temurin-17.0.2+8 (build 17.0.2+8)
 - SpringBoot version: 3.0.5
 - Maven version: 3.6.3

## NodeJS

 - npm 8.11.0
 - node v16.15.1
 - yarn 1.22.19

# Source code



////////////////////////////////

Prerequisites:
 - the simplest setup
 - web app with controller
 - read/write db
 - read/write kafka
 - TypeScript + NestJS + Yarn
 - Java + SpringBoot + mvn
 - Dockerfile + docker-compose

Metrics:
 - lines of code
 - image size
 - memory & cpu usage?

Tools & versions:
 - sloc

Steps:
1. Basic controller + service, logger
2. Unit tests
3. Integration tests
4. Dockerize
5. Adding ORM
6. Integration tests
7. Adding Kafka
8. Integration tests